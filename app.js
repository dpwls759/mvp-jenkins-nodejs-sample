//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cookieParser = require('cookie-parser');
//----------

//--- global constants & 환경변수
global.__BASEDIR = __dirname + '/';
const port = (process.env.PORT || 8090);
//--------

//---- 기본 library 셋팅
const app = express();
app.use(express.static(path.join(__BASEDIR, '/public')));		//static resource 폴더 
app.use(bodyParser.urlencoded({extended:false}));				//include request 객체 parser
//app.use(express.bodyParser());
app.use(cookieParser());										//include cookie parser
//-----------

//--- include 개발 모듈
app.use(require(path.join(__BASEDIR, "/routes/sample.js")));		//include 상품정보처리
//--------

//----- start web server 
app.listen(port, () => {
	console.log('Listen: ' + port);
});
//----------------
